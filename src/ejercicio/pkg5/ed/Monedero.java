/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio.pkg5.ed;

/**
 *
 * @author DAM126
 */
public class Monedero {

    private double cantidad;

   
    public Monedero(double cantidad) {
        this.cantidad = cantidad;
    }

    public void introducirDinero(double dinero) {
        cantidad = dinero + cantidad;
        System.out.println("Ahora tienes en tu monedero "+cantidad);
    }

    public void sacarDinero(double dinero) {
        if (cantidad < dinero) {
            System.out.println("No tienes suficiente dinero");
        } else {
            cantidad = cantidad - dinero;
        }
        }

    public double getCantidad() {
        return cantidad;
    }

}
